import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {AuthService} from '../auth.service';
import {get} from 'lodash-es';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	constructor(
		private configService: ConfigService,
		private authService: AuthService,
		private snackBar: MatSnackBar
	) {
	}

	public config: XanoConfig;
	public configured: boolean = false;
	public redirectUri: string;

	ngOnInit(): void {
		this.config = this.configService.config;
		this.redirectUri = this.configService.redirectUri;
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl);
	}

	public initFacebook(route): void {
		this.authService.facebookInit().subscribe(url => {
			console.log(url);
			localStorage.setItem('xano_facebook_demo_route', route);
			localStorage.setItem('xano_api_url', this.configService.xanoApiUrl.value);
			window.open(url, '_self');
		}, error => {
			this.snackBar.open(get(error, 'error.message', 'An error occurred'), 'Error', {panelClass: 'error-snack'});
		});
	}

}
