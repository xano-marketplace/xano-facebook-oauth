import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {ConfigPanelComponent} from '../config-panel/config-panel.component';

@Component({
	selector: 'app-action-bar',
	templateUrl: './action-bar.component.html',
	styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {
	public config: XanoConfig;
	public isConfigured: boolean = false;

	constructor(
		private configService: ConfigService,
		private router: Router,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(res => this.isConfigured = res);
		this.config = this.configService.config;
	}


	public showPanel(dialogType): void {
		const dialogRef = this.dialog.open(ConfigPanelComponent);
	}

}
