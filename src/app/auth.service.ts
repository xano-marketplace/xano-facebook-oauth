import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ConfigService} from './config.service';
import {ApiService} from './api.service';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(private configService: ConfigService, private apiService: ApiService) {
	}

	public facebookInit(): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/facebook/init`,
			params: {
				redirect_uri: this.configService.redirectUri
			}
		});
	}

	public facebookLogin(code): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/facebook/login`,
			params: {
				redirect_uri: this.configService.redirectUri,
				code: code
			}
		});
	}

	public facebookSignUp(code): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/facebook/signup`,
			params: {
				redirect_uri: this.configService.redirectUri,
				code: code
			}
		});
	}

	public facebookContinue(code): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/facebook/continue`,
			params: {
				redirect_uri: this.configService.redirectUri,
				code: code
			}
		});
	}
}
